#!/usr/bin/bash

cd "./routes-native"
cargo build --release
cd "../"

cp "./routes-native/target/release/libroutes_native.so" "./godot/native/" -i
