extends Camera2D
export var min_zoom = 0.4
export var speed = 200
var zoomlevel = 0.4


func _process(delta):
	move_around(delta)
	handle_zoom()

func move_around(delta):
	var xdirection = Input.get_action_strength("camera_right") - Input.get_action_strength("camera_left")
	var ydirection = Input.get_action_strength("camera_down") - Input.get_action_strength("camera_up")
	var direction = Vector2(xdirection, ydirection).normalized() * delta * speed * zoom
	position = lerp(position, position + direction, 2)

func handle_zoom():
	if Input.is_action_just_pressed("zoom_in"):
		zoomlevel = clamp(zoomlevel - 0.1, 0.1, 2)
	if Input.is_action_just_pressed("zoom_out"):
		zoomlevel = clamp(zoomlevel + 0.1, 0.1, 2)
	zoom = Vector2(zoomlevel, zoomlevel)
