use rand::SeedableRng;

pub type GameRng = rand_xoshiro::Xoshiro256PlusPlus;

pub fn get_rng(seed: impl AsRef<[u8]> /* implemented by String */) -> GameRng {
    // blake3::Hash is a [u8; 32] under the hood and implements From and Into to convert to and from it
    let hash: [u8; 32] = blake3::hash(seed.as_ref()).into();
    // Xoshiro256++ seeds are [u8; 32] :3
    GameRng::from_seed(hash)
}
