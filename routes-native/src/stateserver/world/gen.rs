use noise::{
    utils::{NoiseMap, NoiseMapBuilder, PlaneMapBuilder},
    Seedable, SuperSimplex,
};
use rand::Rng;
pub use crate::stateserver::gen::GameRng;


pub fn get_noise(rng: &mut GameRng, size: (usize, usize)) -> NoiseMap {
    // rng.gen::<u32>() generates a random u32 which is already between 0 and u32::MAX
    let noise = SuperSimplex::new().set_seed(rng.gen());
    PlaneMapBuilder::new(&noise)
        .set_size(size.0, size.1)
        .build()
}
