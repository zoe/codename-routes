use gdnative::prelude::*;

mod stateserver;

fn init(handle: InitHandle) {
    handle.add_class::<stateserver::StateServer>();
}

godot_init!(init);
