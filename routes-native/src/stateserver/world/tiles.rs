use derive_builder::Builder;
use strum::{AsRefStr, EnumCount, EnumDiscriminants, EnumIter, FromRepr, IntoEnumIterator};

#[derive(AsRefStr, EnumIter, FromRepr, EnumDiscriminants, Clone, Copy, PartialEq, EnumCount)]
#[repr(u8)]
pub enum Tiletypes {
    Air,
    Water,
    Grass,
    Dirt,
    Sand,
    Rock,
    WaterSlab,
    GrassSlab,
    SandPathTlBr,
    SandPathTrBl,
    SandPathCross,
    SandPathCurveDD,
    SandPathCurveUU,
    SandPathCurveLL,
    SandPathCurveRR,
    SandPathTTlBr,
    SandPathTrBlBr,
    SandPathTrTlBl,
    SandPathTlTrBr,

}

#[allow(dead_code)]
#[derive(Builder)]
pub struct Tile {
    kind: Tiletypes,
    #[builder(default = "true")]
    pub is_support: bool,
    #[builder(default = "true")]
    pub hide_top_left: bool,
    #[builder(default = "true")]
    pub hide_top_right: bool,
    #[builder(default = "true")]
    pub hide_below: bool,
    #[builder(default = "false")]
    pub must_be_on_top: bool,
    #[builder(default = "true")]
    pub can_be_bedrock: bool,
    #[builder(default = "false")]
    pub is_grass: bool,
}

impl Tile {
    pub fn new(kind: Tiletypes) -> Tile {
        let tile: Tile;
        match kind {
            Tiletypes::Air => {
                tile = TileBuilder::default()
                    .kind(kind)
                    .is_support(false)
                    .hide_below(false)
                    .hide_top_right(false)
                    .hide_top_left(false)
                    .can_be_bedrock(false)
                    .build()
                    .unwrap()
            }
            Tiletypes::Water => {
                tile = TileBuilder::default()
                    .kind(kind)
                    .is_support(false)
                    .build()
                    .unwrap()
            }
            Tiletypes::Grass => {
                tile = TileBuilder::default()
                    .kind(kind)
                    .must_be_on_top(true)
                    .is_grass(true)
                    .build()
                    .unwrap()
            }
            Tiletypes::GrassSlab => {
                tile = TileBuilder::default()
                    .kind(kind)
                    .must_be_on_top(true)
                    .is_support(false)
                    .is_grass(true)
                    .build()
                    .unwrap()
            }
            Tiletypes::WaterSlab => {
                tile = TileBuilder::default()
                    .kind(kind)
                    .is_support(false)
                    .build()
                    .unwrap()
            }
            _ => tile = TileBuilder::default().kind(kind).build().unwrap(),
        };
        tile
    }
    pub fn kind_to_string(&self) -> String {
        self.kind.as_ref().to_string()
    }
}

pub struct Attributelists {
    pub bedrock: Vec<Tiletypes>,
    pub grass: Vec<Tiletypes>,
}

impl Attributelists {
    pub fn new() -> Attributelists{
    let mut bedrock: Vec<Tiletypes> = vec![];
    let mut grass: Vec<Tiletypes> = vec![];
        for tiletype in Tiletypes::iter(){
            let tile = Tile::new(tiletype);
            if tile.can_be_bedrock { bedrock.push(tiletype);
            if tile.is_grass {grass.push(tiletype);}
            }
        }
    Attributelists { bedrock, grass }
    }
}
