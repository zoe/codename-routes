use gdnative::prelude::*;

use self::gen::GameRng;

mod world;
mod gen;

#[derive(NativeClass)]
#[inherit(Node)]
#[register_with(Self::register)]
pub struct StateServer {
    rng: GameRng,
    world: world::World,
}

#[methods]
impl StateServer {
    fn new(_owner: &Node) -> Self {
        StateServer {
            world: world::World::new(0, 0, 0),
            rng: gen::get_rng("seed".to_string())
        }
    }

    #[export]
    fn _ready(&self, _owner: &Node) {}

    #[export]
    fn generate_world(&mut self, _owner: &Node, xsize: usize, ysize: usize, zsize: usize, seed: String) {
        self.rng = gen::get_rng(seed);
        self.world = world::World::new(xsize, ysize, zsize);
        let w = self.world.generate(&mut self.rng);
        _owner.emit_signal("request_init", &[]);
        _owner.emit_signal("changed_tiletypes", &[Variant::new(&w)]);
    }

    #[export]
    fn get_world_size(&self, _ownser: &Node) -> Vector3 {
        Vector3::new(
            self.world.xsize as f32,
            self.world.ysize as f32,
            self.world.zsize as f32,
        )
    }

    #[export]
    fn get_tile_at(&self, _owner: &Node, x: usize, y: usize, z: usize) -> u16 {
        self.world.get_tile_at(x, y, z)
    }

    #[export]
    fn is_tile_hidden(&self, _owner: &Node, x: usize, y: usize, z: usize) -> bool {
        self.world.is_tile_hidden(x, y, z)
    }

    #[export]
    fn can_put_tile_at(&self, _owner: &Node, x: usize, y: usize, z: usize, id: usize) -> bool{
        self.world.can_put_tile_at(x, y, z, id)
    }

    #[export]
    fn put_tile_at(&mut self, _owner: &Node, x: usize, y: usize, z: usize, id: usize) {
        let changed = self.world.put_tile_at(x, y, z, id);
        _owner.emit_signal("changed_tiletypes", &[Variant::new(&changed)]);
    }
}

// signals
impl StateServer {
    fn register(builder: &ClassBuilder<StateServer>) {
        builder
            .signal("changed_tiletypes")
            .with_param("tile_positions", VariantType::Vector3Array)
            .done();
        builder.signal("request_init").done();
    }
}
