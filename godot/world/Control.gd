extends Control

signal reload_requested

func _on_Reload_pressed():
	emit_signal("reload_requested", $TextEdit.text)
