extends Node

enum TOOLS {PLACE}

var active_tool = TOOLS.PLACE
var active_height: int = 0
var Tilemap = preload("res://world/Tilemap.tscn")
var tilemaps: Array = []

func _process(_delta):
	if Input.is_action_just_pressed("use_tool"):
		use_tool_on_active_tile()
	if Input.is_action_just_pressed("tool_1"):
		switch_to_action(TOOLS.PLACE)
	if Input.is_action_just_pressed("height_down"):
		change_height(-1)
	elif Input.is_action_just_pressed("height_up"):
		change_height(1)

func _ready():
	StateServer.connect("changed_tiletypes", self, "_on_StateServer_changed_tiletypes")
	StateServer.connect("request_init", self, "_on_StateServer_request_init")

func change_height(v: int):
	active_height = int(clamp(active_height + v, 0, tilemaps.size() - 1))
	for i in range(0, tilemaps.size()):
		if i > active_height:
			tilemaps[i].modulate.a = 0.12
		else: tilemaps[i].modulate.a = 1

func respawn_tilemaps():
	tilemaps = []
	for i in range(0, StateServer.get_world_size().z):
		var map = Tilemap.instance()
		map.position.y -= i * map.cell_size.y
		map.z_index = i
		add_child(map)
		tilemaps.push_back(map)
		change_height(0)

func get_tile_at(pos: Vector3) -> String:
	return StateServer.get_tile_at(int(pos.x), int(pos.y), int(pos.z))

func update_tiles(tile_positions: PoolVector3Array):
	for tile in tile_positions:
		tilemaps[tile.z].set_tile_graphics(Vector2(tile.x, tile.y),
			get_tile_at(tile))
	for map in tilemaps:
		map.update_dirty_quadrants()

func switch_to_action(action):
	if action == TOOLS.PLACE:
		active_tool = TOOLS.PLACE
		print(action)

func get_mouseover_tile() -> Vector2:
	var mouse_pos = tilemaps[active_height].get_local_mouse_position()
	return tilemaps[active_height].world_to_map(mouse_pos)

func use_tool_on_active_tile():
	var tile = get_mouseover_tile()
	StateServer.put_tile_at(int(tile.x), int(tile.y), active_height, 1)

func _on_StateServer_request_init():
	call_deferred("respawn_tilemaps")

func _on_StateServer_changed_tiletypes(tile_positions: PoolVector3Array):
	call_deferred("update_tiles", tile_positions)

