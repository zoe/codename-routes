extends TileMap

func set_tile_graphics(pos: Vector2, id: int):
	if id == 0: set_cellv(pos, -1)
	set_cellv(pos,id,false,false,false,get_variant())

func get_variant() -> Vector2:
	return Vector2(randi() % 4, 0)
